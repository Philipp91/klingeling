package de.philippkeck.klingeling;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Arrays;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

public class Sender {
	
	/**
	 * WICHTIG: Wenn's mal nicht mehr tut, einfach den Router neustarten.
	 */

	private static final String TAG = "Sender";
	private static final int REPEAT_COUNT = 10;
	private static final int SEND_TIME_SPAN = 30 * 1000; 

	public static AsyncTask<Void, Void, Void> sendMessage(final Context context, final InetAddress destination, final String message, final int id, final String... parameters) {
		final long timeLimit = System.currentTimeMillis() + SEND_TIME_SPAN;
		Log.i(TAG, "Starting Thead for message sending " + id + " " + Arrays.toString(parameters));
		return new AsyncTask<Void, Void, Void>() {
			protected Void doInBackground(Void... params) {
				Log.i(TAG, "Sending message");
				StringBuilder data = new StringBuilder();
				data.append(message);
				data.append("|");
				data.append(id);
				for (String parameter : parameters) {
					data.append("|");
					data.append(parameter);
				}
				sendUDP(context, destination, data.toString(), timeLimit);
				return null;
			}
		}.execute();
	}

	// destination==null hei�t "Broadcast", d.h. an alle im lokalen Netz mit
	// Maske "255.255.255.0"
	private static boolean sendUDP(Context context, InetAddress destination, String data, long timeLimit) {
		try {
			MulticastSocket s = new MulticastSocket();
			byte[] bytdat = data.getBytes("ascii");
			if (destination == null) {

				WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
				WifiInfo wifiInfo = wifiManager.getConnectionInfo();
				if (wifiInfo == null) {
					return false;
				}

				int myIP = wifiInfo.getIpAddress();
				byte[] address = new byte[] { (byte) (myIP & 0xff), (byte) (myIP >> 8 & 0xff), (byte) (myIP >> 16 & 0xff), (byte) (myIP >> 24 & 0xff) };
				byte ownSuffix = address[3];
				Log.i(TAG, "Broadcasting. Own address: " + Arrays.toString(address));

				for (int repeat = 0; repeat < REPEAT_COUNT; repeat++) {
					for (byte i = Byte.MIN_VALUE; i < Byte.MAX_VALUE; i++) {
						if (i == ownSuffix || i == 0 || i == 1) {
							continue;
						}

						try {
							address[3] = i;
							DatagramPacket pack = new DatagramPacket(bytdat, bytdat.length, InetAddress.getByAddress(address), ReceiverService.KLINGEL_PORT);
							s.send(pack);
							Thread.sleep(1);
						} catch (Exception ex) {
							Log.w(TAG, "Could not send to " + Arrays.toString(address), ex);
						}
					}

					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
					}
					
					if (System.currentTimeMillis() > timeLimit) {
						break;
					}
				}
			} else {
				Log.i(TAG, "Sending to: " + destination);
				DatagramPacket pack = new DatagramPacket(bytdat, bytdat.length, destination, ReceiverService.KLINGEL_PORT);
				s.send(pack);
			}
			s.close();
			return true;
		} catch (IOException ex) {
			return false;
		}
	}
}
