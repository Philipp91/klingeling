package de.philippkeck.klingeling;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class ReceiverService extends Service {
	
	/**
	 * WICHTIG: Wenn's mal nicht mehr tut, einfach den Router neustarten.
	 */

	private static final String TAG = "ReceiverService";
	
	public static WeakReference<KlingelActivity> currentActivity;

	public static final int KLINGEL_PORT = 11337;
	public static final String KLINGEL_MESSAGE = "Klingeling";
	public static final String EMPFANGEN_MESSAGE = "Empfangen";
	public static final String GELESEN_MESSAGE = "Gelesen";
	public static final String RESPONSE_OK = "OK";
	public static final String RESPONSE_GLEICH = "Gleich";
	public static final String RESPONSE_NEIN = "Nein";
	public static final int MAX_RUF_ID = 1000000;

	private final IBinder mBinder = new Binder();
	
	@SuppressLint("UseSparseArrays")
	private final Map<Integer, Long> confirmedRufIDs = new HashMap<Integer, Long>();
	
	private DatagramSocket socket;
	private final Runnable listenerRunnable = new Runnable() {
		public void run() {
			try {
				Log.i(TAG, "Opening socket");
				if (socket != null) {
					destroySocket();
				}
				
				socket = new DatagramSocket(KLINGEL_PORT);
				socket.setReuseAddress(true);
				try {
					while (true) {
						byte[] data = new byte[1024];
						DatagramPacket packet = new DatagramPacket(data, data.length);
						Log.i(TAG, "Listening for messages");
						socket.receive(packet);
						String strMessage = new String(packet.getData(), packet.getOffset(), packet.getLength());
						Log.i(TAG, "Received Message: " + strMessage);
						
						String[] message = strMessage.split("\\|");
						if (message.length >= 3 && message[2].equals(myUserName)) {
							// Die ist von mir selber => ignorieren
							continue;
						}
						if (message[0].equals(KLINGEL_MESSAGE) && message.length == 3) {
							final int id = Integer.parseInt(message[1]);
							if (!confirmedRufIDs.containsKey(id) || confirmedRufIDs.get(id) < System.currentTimeMillis() - KlingelActivity.EXPIRE_TIME) {
								confirmedRufIDs.put(id, System.currentTimeMillis());
								showNotification(id, message[2], packet.getAddress());
							}
							Sender.sendMessage(getApplicationContext(), packet.getAddress(), EMPFANGEN_MESSAGE, id, myUserName);
							Log.i(TAG, "Empfangsbestätigung verschickt");
							
						} else if (message[0].equals(EMPFANGEN_MESSAGE) && message.length == 3) {
							Log.i(TAG, "Empfangsbestätigung von " + message[2] + " empfangen " + message[1]);
							if (currentActivity != null) {
								currentActivity.get().onEmpfangen(Integer.parseInt(message[1]), message[2]);
							} else {
								Log.i(TAG, "currentActivity war null");
							}
							
						} else if (message[0].equals(GELESEN_MESSAGE) && message.length >= 3) {
							Log.i(TAG, "Lesebestätigung von " + message[2] + " empfangen " + message[1]);
							if (currentActivity != null) {
								String response = message.length == 3 ? RESPONSE_OK : message[3];
								currentActivity.get().onGelesen(Integer.parseInt(message[1]), message[2], response);
							} else {
								Log.i(TAG, "currentActivity war null");
							}
						} else {
							Log.w(TAG, "Did not understand message: " + strMessage);
						}
					}
				} finally {
					destroySocket();
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				listenerThread = null;
				destroySocket();
			}
		}
	};

	private Thread listenerThread;
	private String myUserName;

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i(TAG, "Destroying service");
		if (listenerThread != null) {
			listenerThread.interrupt();
		}
		listenerThread = null;
		destroySocket();
	}
	
	private void destroySocket() {
		Log.i(TAG, "Destroying socket");
		if (socket != null) {
			try {
				socket.close();
			} catch (Exception e) {
			}
			socket = null;
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (listenerThread == null) {
			myUserName = Utils.getUserName(this);
			Log.i(TAG, "Starting service");
			listenerThread = new Thread(listenerRunnable);
			listenerThread.start();
		}
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@SuppressLint("InlinedApi")
	private void showNotification(int rufID, String name, InetAddress destination) {
		
		Uri soundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.sound);
		
		Notification notification = new NotificationCompat.Builder(getApplicationContext())
			.setSmallIcon(R.drawable.clock)
			.setContentTitle(getString(R.string.notification_title))
			.setContentText(name + " " + getString(R.string.notification_text))
			.setSound(soundUri)
			.setPriority(Notification.PRIORITY_MAX)
			.setDefaults(Notification.DEFAULT_VIBRATE)
			.setLights(0xFFFF0000, 2000, 1000)
			.setDeleteIntent(createPendingIntent(rufID, RESPONSE_OK, 0, destination))
			.addAction(R.drawable.notif_check, getString(R.string.action_ok), createPendingIntent(rufID, RESPONSE_OK, 1, destination))
			.addAction(R.drawable.notif_clock, getString(R.string.action_gleich), createPendingIntent(rufID, RESPONSE_GLEICH, 2, destination))
			.addAction(R.drawable.notif_cross, getString(R.string.action_nein), createPendingIntent(rufID, RESPONSE_NEIN, 3, destination))
			.build();
		
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.notify(rufID, notification);
	}
	
	private PendingIntent createPendingIntent(int rufID, String response, int messageType, InetAddress destination) {
		Intent intent = new Intent(this, EventReceiver.class);
		intent.setAction(EventReceiver.ACTION_NOTIFICATION_DELETED);
		intent.putExtra("rufID", rufID);
		intent.putExtra("response", response);
		intent.putExtra("destination", destination);
		return PendingIntent.getBroadcast(getApplicationContext(), rufID + messageType * MAX_RUF_ID, intent, 0);
	}
	
}