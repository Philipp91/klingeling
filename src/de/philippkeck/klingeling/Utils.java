package de.philippkeck.klingeling;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.ContactsContract;

public final class Utils {

	private Utils() {
	}

	public static boolean isWifiConnected(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		if (networkInfo == null) {
			return false;
		} else {
			return networkInfo.getType() == ConnectivityManager.TYPE_WIFI && networkInfo.isConnected();
		}
	}

	public static String getUserName(Context context) {
		final ContentResolver contentResolver = context.getContentResolver();
		final Cursor cursor = contentResolver.query(
				// Retrieves data rows for the device user's 'profile' contact
				Uri.withAppendedPath(
						ContactsContract.Profile.CONTENT_URI, 
						ContactsContract.Contacts.Data.CONTENT_DIRECTORY
				),
				new String[] { 
					ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME
				},

				ContactsContract.Contacts.Data.MIMETYPE + "=?",
				new String[] { 
					ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, 
				},

				ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");

		String result = null;
		if (cursor.moveToNext()) {
			result = cursor.getString(0);
		}
		cursor.close();

		return result;
	}

}
