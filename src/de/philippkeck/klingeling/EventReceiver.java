package de.philippkeck.klingeling;

import java.net.InetAddress;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

public class EventReceiver extends BroadcastReceiver {

	private static final String TAG = "EventReceiver";
	public static final String ACTION_NOTIFICATION_DELETED = "de.philippkeck.klingeling.ACTION_NOTIFICATION_DELETED";

	@Override
	public void onReceive(Context context, Intent intent) {

		if (intent.getAction().equals(ACTION_NOTIFICATION_DELETED)) {
			int rufID = intent.getIntExtra("rufID", -1);
			InetAddress destination = (InetAddress) intent.getSerializableExtra("destination"); 
			if (rufID == -1) {
				Log.w(TAG, "RufID war nicht im Intent!");
			} else {
				String name = Utils.getUserName(context);
				String response = intent.getStringExtra("response");
				Log.i(TAG, "User dismissed notification, sending read confirmation " + rufID + " " + response + " " + name);
				Sender.sendMessage(context, destination, ReceiverService.GELESEN_MESSAGE, rufID, name, response);
				
				NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				notificationManager.cancel(rufID);
			}
			
		} else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED) || intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
			if (Utils.isWifiConnected(context)) {
				Log.i(TAG, "Received signal, starting service");
				context.startService(new Intent(context, ReceiverService.class));
			} else {
				Log.i(TAG, "Received signal, stopping service");
				context.stopService(new Intent(context, ReceiverService.class));
			}
		} else {
			Log.w(TAG, "Unknown Action " + intent.getAction());
		}
	}

}
