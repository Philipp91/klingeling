package de.philippkeck.klingeling;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class KlingelActivity extends Activity {

	private static final String TAG = "KlingelActivity";
	public static final long EXPIRE_TIME = 1000 * 60 * 60; // 1h

	private int rufID;
	private long rufTime;
	private AsyncTask<Void, Void, Void> sendTask;

	private ArrayList<Recipient> recipients = new ArrayList<Recipient>();
	private Map<String, Recipient> mapRecipients = new HashMap<String, Recipient>();
	private RecipientsListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setTitle(R.string.klingel_title);
		ReceiverService.currentActivity = new WeakReference<KlingelActivity>(this);
		setContentView(R.layout.activity_klingel);

		if (Utils.isWifiConnected(this)) {
			Log.i(TAG, "Starting service");
			startService(new Intent(this, ReceiverService.class));
		}

		if (savedInstanceState != null && savedInstanceState.containsKey("rufID")) {
			rufID = savedInstanceState.getInt("rufID");
			rufTime = savedInstanceState.getLong("rufTime");
			recipients = savedInstanceState.getParcelableArrayList("recipients");
			for (Recipient recipient : recipients) {
				mapRecipients.put(recipient.name, recipient);
			}
		} else {
			// Direkt beim �ffnen den Essensruf absenden
			sendNewKlingelRuf();
		}

		adapter = new RecipientsListAdapter();
		ListView lstRecipients = (ListView) findViewById(R.id.lstRecipients);
		lstRecipients.setAdapter(adapter);
		lstRecipients.setEmptyView(findViewById(R.id.emptyView));

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (rufTime + EXPIRE_TIME < System.currentTimeMillis()) {
			// Lezter Klingelruf ist abgelaufen => Neuen senden
			Log.i(TAG, "onResume Ruf ist abgelaufen, sende neuen");
			sendNewKlingelRuf();
			adapter.notifyDataSetChanged();
		}
	}

	private void sendNewKlingelRuf() {
		rufID = (int) (Math.random() * (double) ReceiverService.MAX_RUF_ID);
		rufTime = System.currentTimeMillis();
		recipients.clear();
		mapRecipients.clear();
		sendKlingelRuf();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("rufID", rufID);
		outState.putLong("rufTime", rufTime);
		outState.putParcelableArrayList("recipients", recipients);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState == null) {
			return;
		}
		rufID = savedInstanceState.getInt("rufID");
		rufTime = savedInstanceState.getLong("rufTime");
		recipients = savedInstanceState.getParcelableArrayList("recipients");
		for (Recipient recipient : recipients) {
			mapRecipients.put(recipient.name, recipient);
		}
		if (adapter == null) {
			adapter = new RecipientsListAdapter();
			ListView lstRecipients = (ListView) findViewById(R.id.lstRecipients);
			lstRecipients.setAdapter(adapter);
			lstRecipients.setEmptyView(findViewById(R.id.emptyView));
		}
		adapter.notifyDataSetChanged();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (ReceiverService.currentActivity != null && ReceiverService.currentActivity.get() == this) {
			ReceiverService.currentActivity = null;
		}
	}
	
	public void cancelSending() {
		AsyncTask<Void, Void, Void> oldSendTask = sendTask;
		sendTask = null;
		if (oldSendTask != null) {
			oldSendTask.cancel(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_klingel_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.erneutSenden:
			Log.i(TAG, "Sende Klingelruf erneut");
			setTitle(R.string.resend_title);
			new AsyncTask<Void, Void, Void>() {
				protected Void doInBackground(Void... params) {

					cancelSending();
					sendTask = Sender.sendMessage(KlingelActivity.this, null, ReceiverService.KLINGEL_MESSAGE, rufID, Utils.getUserName(KlingelActivity.this));

					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
					}
					runOnUiThread(new Runnable() {
						public void run() {
							setTitle(R.string.klingel_title);
						}
					});
					return null;
				}
			}.execute();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void sendKlingelRuf() {
		sendTask = Sender.sendMessage(this, null, ReceiverService.KLINGEL_MESSAGE, rufID, Utils.getUserName(KlingelActivity.this));
	}

	public void onEmpfangen(int rufID, final String name) {
		if (rufID == this.rufID) {
			runOnUiThread(new Runnable() {
				public void run() {
					Recipient recipient = mapRecipients.get(name);
					if (recipient == null) {
						recipient = new Recipient(name);
						mapRecipients.put(name, recipient);
						recipients.add(recipient);
						adapter.notifyDataSetChanged();
					}
				}
			});
		} else {
			Log.i(TAG, "RufID " + rufID + " stimmt nicht mit aktueller " + this.rufID + " �berein");
		}
	}

	public void onGelesen(int rufID, final String name, final String response) {
		if (rufID == this.rufID) {
			runOnUiThread(new Runnable() {
				public void run() {
					Recipient recipient = mapRecipients.get(name);
					if (recipient == null) {
						recipient = new Recipient(name);
						mapRecipients.put(name, recipient);
						recipients.add(recipient);
					}
					Log.i(TAG, "Setze Status von " + name + " auf " + response);
					if (response.equals(ReceiverService.RESPONSE_GLEICH)) {
						recipient.response = 2;
					} else if (response.equals(ReceiverService.RESPONSE_NEIN)) {
						recipient.response = 3;
					} else {
						recipient.response = 1;
					}
					adapter.notifyDataSetChanged();
				}
			});
		} else {
			Log.i(TAG, "RufID " + rufID + " stimmt nicht mit aktueller " + this.rufID + " �berein");
		}
	}

	private static class Recipient implements Parcelable {

		@SuppressWarnings("unused")
		public static final Parcelable.Creator<Recipient> CREATOR = new Creator<KlingelActivity.Recipient>() {

			@Override
			public Recipient[] newArray(int size) {
				return new Recipient[size];
			}

			@Override
			public Recipient createFromParcel(Parcel source) {
				Recipient recipient = new Recipient(source.readString());
				recipient.response = source.readInt();
				return recipient;
			}
		};

		public final String name;
		public int response; // 0=empfangen, 1=ok, 2=gleich, 3=nein

		public Recipient(String name) {
			this.name = name;
		}

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(name);
			dest.writeInt(response);
		}
	}

	private class RecipientsListAdapter extends BaseAdapter {

		private LayoutInflater mInflater;

		public RecipientsListAdapter() {
			mInflater = LayoutInflater.from(KlingelActivity.this);
		}

		@Override
		public int getCount() {
			return recipients.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Recipient recipient = recipients.get(position);
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.list_item_icon_text, null);
			}
			((TextView) convertView.findViewById(android.R.id.text1)).setText(recipient.name);

			switch (recipient.response) {
			case 0:
				((TextView) convertView.findViewById(android.R.id.text2)).setText(R.string.text_no_response);
				((ImageView) convertView.findViewById(android.R.id.icon)).setImageResource(R.drawable.user);
				break;
			case 1:
				((TextView) convertView.findViewById(android.R.id.text2)).setText(R.string.text_response_ok);
				((ImageView) convertView.findViewById(android.R.id.icon)).setImageResource(R.drawable.user_check);
				break;
			case 2:
				((TextView) convertView.findViewById(android.R.id.text2)).setText(R.string.text_response_gleich);
				((ImageView) convertView.findViewById(android.R.id.icon)).setImageResource(R.drawable.user_clock);
				break;
			case 3:
				((TextView) convertView.findViewById(android.R.id.text2)).setText(R.string.text_response_nein);
				((ImageView) convertView.findViewById(android.R.id.icon)).setImageResource(R.drawable.user_cross);
				break;
			}
			return convertView;
		}

	}

}
